import helpers::Script
import std::env
import std::sys::(self, Command, ExitStatus, Stream)
import std::test::Tests

fn pub tests(t: mut Tests) {
  t.test('sys.unix?') fn (t) {
    t.equal(sys.unix?, sys.linux? or sys.mac?)
    t.not_equal(sys.unix?, sys.windows?)
  }

  t.test('sys.cpu_cores') fn (t) {
    t.true(sys.cpu_cores > 0)
  }

  t.test('sys.exit') fn (t) {
    let code = '
      sys.exit(4)
      STDOUT.new.print("hello")
    '

    let output = Script.new(t.id, code).import('std::sys').run

    t.false(output.contains?('hello'))
  }

  t.test('Stream.to_int') fn (t) {
    t.equal(Stream.Null.to_int, 0)
    t.equal(Stream.Inherit.to_int, 1)
    t.equal(Stream.Piped.to_int, 2)
  }

  t.test('Command.program') fn (t) {
    t.equal(Command.new('ls').program, 'ls')
  }

  t.test('Command.directory') fn (t) {
    let cmd = Command.new('ls')

    t.equal(cmd.current_directory, Option.None)
    cmd.directory('/foo')
    t.equal(cmd.current_directory, Option.Some('/foo'))
  }

  t.test('Command.argument') fn (t) {
    let cmd = Command.new('ls')

    t.equal(cmd.current_arguments, [])
    cmd.argument('foo')
    t.equal(cmd.current_arguments, ['foo'])
  }

  t.test('Command.arguments') fn (t) {
    let cmd = Command.new('ls')

    t.equal(cmd.current_arguments, [])
    cmd.arguments(['foo'])
    t.equal(cmd.current_arguments, ['foo'])
  }

  t.test('Command.variable') fn (t) {
    let cmd = Command.new('ls')

    t.equal(cmd.current_variables, Map.new)
    cmd.variable('TEST', 'foo')
    t.equal(cmd.current_variables['TEST'], 'foo')
  }

  t.test('Command.variables') fn (t) {
    let cmd = Command.new('ls')
    let vars = Map.new

    vars['TEST'] = 'foo'

    t.equal(cmd.current_variables, Map.new)
    cmd.variables(vars)
    t.equal(cmd.current_variables['TEST'], 'foo')
  }

  t.test('Command.spawn with a valid command') fn (t) {
    let exe = try! env.executable
    let cmd = Command.new(exe)

    cmd.stdin(Stream.Null)
    cmd.stderr(Stream.Null)
    cmd.stdout(Stream.Piped)
    cmd.argument('--help')

    let child = try! cmd.spawn
    let status = try! child.wait
    let bytes = ByteArray.new

    try! child.stdout.read_all(bytes)

    t.true(bytes.into_string.contains?('Usage: inko'))
  }

  t.test('Command.spawn with an invalid command') fn (t) {
    let cmd = Command.new('inko-test-invalid')

    t.throw fn { try cmd.spawn }
  }

  t.test('ExitStatus.to_int') fn (t) {
    t.equal(ExitStatus.new(42).to_int, 42)
  }

  t.test('ExitStatus.success?') fn (t) {
    t.true(ExitStatus.new(0).success?)
    t.false(ExitStatus.new(4).success?)
  }
}
