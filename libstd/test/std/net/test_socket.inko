import helpers::(fmt)
import std::drop::Drop
import std::env
import std::fs::file
import std::fs::path::Path
import std::net::ip::(IpAddress, Ipv4Address, Ipv6Address)
import std::net::socket::(
  Socket, SocketAddress, TcpServer, TcpClient, Type, UdpSocket, UnixAddress,
  UnixDatagram, UnixServer, UnixSocket, UnixClient
)
import std::string::ToString
import std::sys
import std::test::Tests
import std::time::(Duration, Instant)

class SocketPath {
  let @path: Path

  fn static pair(id: Int) -> (Self, Self) {
    let p1 = env.temporary_directory.join("inko-test-{id}-1.sock")
    let p2 = env.temporary_directory.join("inko-test-{id}-2.sock")

    try file.remove(p1) else nil
    try file.remove(p2) else nil

    (Self { @path = p1 }, Self { @path = p2 })
  }

  fn static new(id: Int) -> Self {
    let path = env.temporary_directory.join("inko-test-{id}.sock")

    try file.remove(path) else nil

    Self { @path = path }
  }
}

impl ToString for SocketPath {
  fn pub to_string -> String {
    @path.to_string
  }
}

impl Drop for SocketPath {
  fn mut drop {
    try file.remove(@path) else nil
  }
}

fn pub tests(t: mut Tests) {
  t.test('SocketAddress.new') fn (t) {
    let addr = SocketAddress.new(address: '127.0.0.1', port: 1234)

    t.equal(addr.ip, Option.Some(IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))))
    t.equal(addr.port, 1234)
  }

  t.test('SocketAddress.==') fn (t) {
    let addr1 = SocketAddress.new(address: '127.0.0.1', port: 1234)
    let addr2 = SocketAddress.new(address: '127.0.0.1', port: 4567)

    t.equal(addr1, addr1)
    t.not_equal(addr1, addr2)
  }

  t.test('SocketAddress.fmt') fn (t) {
    t.equal(
      fmt(SocketAddress.new(address: '127.0.0.1', port: 1234)),
      '127.0.0.1:1234'
    )
  }

  t.test('Socket.ipv4') fn (t) {
    t.no_throw fn { try Socket.ipv4(Type.STREAM) }
    t.no_throw fn { try Socket.ipv4(Type.DGRAM) }
  }

  t.test('Socket.ipv6') fn (t) {
    t.no_throw fn { try Socket.ipv6(Type.STREAM) }
    t.no_throw fn { try Socket.ipv6(Type.DGRAM) }
  }

  t.test('Socket.bind') fn (t) {
    t.throw fn {
      let sock = try! Socket.ipv4(Type.STREAM)

      try sock.bind(ip: 'foo', port: 0)
    }

    t.no_throw fn {
      let sock = try! Socket.ipv4(Type.STREAM)

      try sock.bind(ip: '0.0.0.0', port: 0)
    }
  }

  t.test('Socket.connect') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream1 = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    t.no_throw fn {
      try stream1.connect(ip: addr.ip.unwrap, port: addr.port.clone)
    }

    let stream2 = try! Socket.ipv4(Type.STREAM)

    t.throw fn {
      # connect() may not immediately raise a "connection refused" error, due
      # to connect() being non-blocking. In this case the "connection refused"
      # error is raised on the next operation.
      #
      # Since a connect() _might_ still raise the error right away, we have to
      # both connect and try to use the socket in some way.
      try stream2.connect(ip: '0.0.0.0', port: 40_000)
      try stream2.write_string('ping')
    }
  }

  t.test('Socket.listen') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    try! socket.bind(ip: '0.0.0.0', port: 0)

    t.no_throw fn { try socket.listen }
  }

  t.test('Socket.accept') fn (t) {
    let server = try! Socket.ipv4(Type.STREAM)
    let client = try! Socket.ipv4(Type.STREAM)

    try! server.bind(ip: '127.0.0.1', port: 0)
    try! server.listen

    let addr = try! server.local_address

    try! client.connect(addr.address.clone, addr.port.clone)

    let connection = try! server.accept

    t.equal(try! connection.local_address, try! server.local_address)
  }

  t.test('Socket.send_string_to') fn (t) {
    let socket = try! Socket.ipv4(Type.DGRAM)

    # On Windows one can not use sendto() with 0.0.0.0 being the target IP
    # address, so instead we bind (and send to) 127.0.0.1.
    try! socket.bind(ip: '127.0.0.1', port: 0)

    let send_to = try! socket.local_address
    let buffer = ByteArray.new

    try! socket
      .send_string_to('ping', ip: send_to.address, port: send_to.port.clone)

    t.equal(try! socket.read(into: buffer, size: 4), 4)
    t.equal(buffer.into_string, 'ping')
  }

  t.test('Socket.send_bytes_to') fn (t) {
    let socket = try! Socket.ipv4(Type.DGRAM)

    # On Windows one can not use sendto() with 0.0.0.0 being the target IP
    # address, so instead we bind (and send to) 127.0.0.1.
    try! socket.bind(ip: '127.0.0.1', port: 0)

    let send_to = try! socket.local_address
    let buffer = ByteArray.new

    try! socket.send_bytes_to(
      bytes: 'ping'.to_byte_array,
      ip: send_to.address,
      port: send_to.port.clone
    )

    t.equal(try! socket.read(into: buffer, size: 4), 4)
    t.equal(buffer.into_string, 'ping')
  }

  t.test('Socket.receive_from') fn (t) {
    let listener = try! Socket.ipv4(Type.DGRAM)
    let client = try! Socket.ipv4(Type.DGRAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! client.bind(ip: '127.0.0.1', port: 0)

    let send_to = try! listener.local_address

    try! client
      .send_string_to('ping', ip: send_to.address, port: send_to.port.clone)

    let bytes = ByteArray.new
    let sender = try! listener.receive_from(bytes: bytes, size: 4)

    t.equal(sender, try! client.local_address)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('Socket.local_address with an unbound socket') fn (t) {
    if sys.windows? {
      let socket = try! Socket.ipv4(Type.DGRAM)

      t.throw fn { try socket.local_address }
    } else {
      let socket = try! Socket.ipv4(Type.DGRAM)
      let address = try! socket.local_address

      t.equal(address.address, '0.0.0.0')
      t.equal(address.port, 0)
    }
  }

  t.test('Socket.local_address with a bound socket') fn (t) {
    let socket = try! Socket.ipv4(Type.DGRAM)

    try! socket.bind(ip: '127.0.0.1', port: 0)

    let local_address = try! socket.local_address

    t.equal(local_address.address, '127.0.0.1')
    t.true(local_address.port > 0)
  }

  t.test('Socket.peer_address with a disconnected socket') fn (t) {
    let socket = try! Socket.ipv4(Type.DGRAM)

    t.throw fn { try socket.peer_address }
  }

  t.test('Socket.peer_address with a connected socket') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let client = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! client.connect(ip: addr.address, port: addr.port.clone)

    t.equal(try! client.peer_address, addr)
  }

  t.test('Socket.ttl') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    try! socket.ttl = 10

    t.equal(try! socket.ttl, 10)
  }

  t.test('Socket.only_ipv6?') fn (t) {
    let socket = try! Socket.ipv6(Type.STREAM)

    try! socket.only_ipv6 = true

    t.true(try! socket.only_ipv6?)
  }

  t.test('Socket.no_delay?') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    try! socket.no_delay = true

    t.true(try! socket.no_delay?)
  }

  t.test('Socket.broadcast?') fn (t) {
    let socket = try! Socket.ipv4(Type.DGRAM)

    try! socket.broadcast = true

    t.true(try! socket.broadcast?)
  }

  t.test('Socket.linger') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)
    let duration = Duration.from_secs(5)

    try! socket.linger = duration

    t.equal(try! socket.linger, duration)
  }

  t.test('Socket.receive_buffer_size') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    try! socket.receive_buffer_size = 256

    t.true(try! { socket.receive_buffer_size } >= 256)
  }

  t.test('Socket.send_buffer_size') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    try! socket.send_buffer_size = 256

    t.true(try! { socket.send_buffer_size } >= 256)
  }

  # Obtaining the TCP keepalive setting fails on Windows. See
  # https://github.com/alexcrichton/socket2-rs/issues/24 for more information.
  if sys.windows?.false? {
    t.test('Socket.keepalive') fn (t) {
      let socket = try! Socket.ipv4(Type.STREAM)

      try! socket.keepalive = true

      t.true(try! socket.keepalive)
    }
  }

  t.test('Socket.reuse_adress') fn (t) {
    let socket = try! Socket.ipv6(Type.DGRAM)

    try! socket.reuse_address = true

    t.true(try! socket.reuse_address)
  }

  t.test('Socket.reuse_port') fn (t) {
    let socket = try! Socket.ipv6(Type.DGRAM)

    try! socket.reuse_port = true

    # Windows does not support SO_REUSEPORT, so the return value is always
    # `false`.
    if sys.windows? {
      t.false(try! socket.reuse_port)
    } else {
      t.true(try! socket.reuse_port)
    }
  }

  t.test('Socket.shutdown_read') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! stream.connect(ip: addr.address, port: addr.port.clone)
    try! stream.shutdown_read

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 0)
    t.equal(bytes.length, 0)
  }

  t.test('Socket.shutdown_write') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! stream.connect(ip: addr.address, port: addr.port.clone)
    try! stream.shutdown_write

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('Socket.shutdown shuts down the writing half') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! stream.connect(ip: addr.address, port: addr.port.clone)

    t.throw fn {
      try stream.shutdown
      try stream.write_string('ping')
    }
  }

  t.test('Socket.shutdown shuts down the reading half') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! stream.connect(ip: addr.address, port: addr.port.clone)
    try! stream.shutdown

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 0)
    t.equal(bytes.length, 0)
  }

  t.test('Socket.try_clone') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    t.no_throw fn { try socket.try_clone }
  }

  t.test('Socket.read') fn (t) {
    let socket = try! Socket.ipv4(Type.DGRAM)

    try! socket.bind(ip: '127.0.0.1', port: 0)

    let addr = try! socket.local_address
    let bytes = ByteArray.new

    try! socket.send_string_to('ping', ip: addr.address, port: addr.port.clone)

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('Socket.write_bytes') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! stream.connect(ip: addr.address, port: addr.port.clone)

    let written = try! stream.write_bytes('ping'.to_byte_array)
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('Socket.write_string') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)
    let stream = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    try! stream.connect(ip: addr.address, port: addr.port.clone)

    let written = try! stream.write_string('ping')
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('Socket.flush') fn (t) {
    let socket = try! Socket.ipv4(Type.STREAM)

    t.equal(socket.flush, nil)
  }

  t.test('UdpSocket.new') fn (t) {
    t.no_throw fn {
      try UdpSocket
        .new(ip: IpAddress.V4(Ipv4Address.new(0, 0, 0, 0)), port: 0)
    }

    t.no_throw fn {
      try! UdpSocket
        .new(ip: IpAddress.V6(Ipv6Address.new(0, 0, 0, 0, 0, 0, 0, 0)), port: 0)
    }
  }

  t.test('UdpSocket.connect') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let socket1 = try! UdpSocket.new(ip: ip.clone, port: 0)
    let socket2 = try! UdpSocket.new(ip: ip, port: 0)
    let addr = try! socket2.local_address

    t.no_throw fn {
      try socket1.connect(ip: addr.address, port: addr.port.clone)
    }
  }

  t.test('UdpSocket.send_string_to') fn (t) {
    let socket = try! UdpSocket
      .new(ip: IpAddress.V4(Ipv4Address.new(127, 0, 0, 1)), port: 0)

    let addr = try! socket.local_address

    try! socket.send_string_to('ping', ip: addr.address, port: addr.port.clone)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UdpSocket.send_bytes_to') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let socket = try! UdpSocket.new(ip: ip, port: 0)
    let addr = try! socket.local_address

    try! socket.send_bytes_to(
      'ping'.to_byte_array,
      ip: addr.address,
      port: addr.port.clone
    )

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UdpSocket.receive_from') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let listener = try! UdpSocket.new(ip: ip.clone, port: 0)
    let client = try! UdpSocket.new(ip: ip, port: 0)
    let addr = try! listener.local_address

    try! client
      .send_string_to('ping', ip: addr.address, port: addr.port.clone)

    let bytes = ByteArray.new
    let sender = try! listener.receive_from(bytes: bytes, size: 4)

    t.equal(sender, try! client.local_address)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UdpSocket.local_address') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let socket = try! UdpSocket.new(ip: ip, port: 0)
    let local_address = try! socket.local_address

    t.equal(local_address.address, '127.0.0.1')
    t.true(local_address.port > 0)
  }

  t.test('UdpSocket.try_clone') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let socket = try! UdpSocket.new(ip: ip, port: 0)

    t.no_throw fn { try socket.try_clone }
  }

  t.test('UdpSocket.read') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let socket = try! UdpSocket.new(ip: ip, port: 0)
    let addr = try! socket.local_address

    try! socket.send_string_to('ping', ip: addr.address, port: addr.port.clone)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.to_string, 'ping')
  }

  t.test('UdpSocket.write_bytes') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let server_socket = try! UdpSocket.new(ip: ip.clone, port: 0)
    let client_socket = try! UdpSocket.new(ip: ip, port: 0)
    let addr = try! server_socket.local_address

    try! client_socket.connect(ip: addr.address, port: addr.port.clone)
    try! client_socket.write_bytes('ping'.to_byte_array)

    let bytes = ByteArray.new

    t.equal(try! server_socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UdpSocket.flush') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let socket = try! UdpSocket.new(ip: ip, port: 0)

    t.equal(socket.flush, nil)
  }

  t.test('TcpClient.new') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    t.no_throw fn {
      try TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    }
  }

  t.test('TcpClient.with_timeout') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address

    t.no_throw fn {
      try TcpClient.with_timeout(
        ip: addr.ip.unwrap,
        port: addr.port.clone,
        timeout_after: Duration.from_secs(2)
      )
    }

    t.throw fn {
      # This address is unroutable and so the connect times out.
      try TcpClient.with_timeout(
        ip: IpAddress.v4(192, 168, 0, 0),
        port: addr.port.clone,
        timeout_after: Duration.from_micros(500)
      )
    }
  }

  t.test('TcpClient.local_address') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    let local_addr = try! stream.local_address

    t.equal(local_addr.address, '127.0.0.1')
    t.true(local_addr.port > 0)
  }

  t.test('TcpClient.peer_address') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    let peer_addr = try! stream.peer_address

    t.equal(peer_addr.address, addr.address)
    t.equal(peer_addr.port, addr.port)
  }

  t.test('TcpClient.read') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    let bytes = ByteArray.new
    let client = try! listener.accept

    try! client.write_string('ping')
    try! stream.read(into: bytes, size: 4)

    t.equal(bytes.into_string, 'ping')
  }

  t.test('TcpClient.write_bytes') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! stream.write_bytes('ping'.to_byte_array), 4)
    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('TcpClient.write_string') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! stream.write_string('ping'), 4)
    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('TcpClient.flush') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)

    t.equal(stream.flush, nil)
  }

  t.test('TcpClient.shutdown_read') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)

    try! stream.shutdown_read

    let bytes = ByteArray.new

    try! stream.read(into: bytes, size: 4)

    t.equal(bytes, ByteArray.new)
  }

  t.test('TcpClient.shutdown_write') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)

    try! stream.shutdown_write

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('TcpClient.shutdown shuts down the writing half') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)

    try! stream.shutdown

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('TcpClient.shutdown shuts down the reading half') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)

    try! stream.shutdown

    let bytes = ByteArray.new

    let message = try! stream.read(into: bytes, size: 4)

    t.equal(bytes, ByteArray.new)
  }

  t.test('TcpClient.try_clone') fn (t) {
    let listener = try! Socket.ipv4(Type.STREAM)

    try! listener.bind(ip: '127.0.0.1', port: 0)
    try! listener.listen

    let addr = try! listener.local_address
    let client = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)

    t.no_throw fn { try client.try_clone }
  }

  t.test('TcpServer.new') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(0, 0, 0, 0))

    t.no_throw fn { try TcpServer.new(ip: ip.clone, port: 0) }

    let listener = try! TcpServer.new(ip: ip, port: 0)
    let addr = try! listener.local_address

    t.no_throw fn {
      try TcpServer.new(ip: addr.ip.unwrap, port: addr.port.clone)
    }
  }

  t.test('TcpServer.accept') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let listener = try! TcpServer.new(ip: ip, port: 0)
    let addr = try! listener.local_address
    let stream = try! TcpClient.new(ip: addr.ip.unwrap, port: addr.port.clone)
    let connection = try! listener.accept

    t.equal(try! connection.local_address, try! stream.peer_address)
  }

  t.test('TcpServer.local_address') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let listener = try! TcpServer.new(ip: ip, port: 0)
    let addr = try! listener.local_address

    t.equal(addr.address, '127.0.0.1')
    t.true(addr.port > 0)
  }

  t.test('TcpServer.try_clone') fn (t) {
    let ip = IpAddress.V4(Ipv4Address.new(127, 0, 0, 1))
    let server = try! TcpServer.new(ip: ip, port: 0)

    t.no_throw fn { try server.try_clone }
  }

  t.test('UnixAddress.to_path') fn (t) {
    t.equal(UnixAddress.new('foo.sock').to_path, Option.Some('foo.sock'.to_path))
    t.true(UnixAddress.new("\0foo").to_path.none?)
    t.true(UnixAddress.new('').to_path.none?)
  }

  t.test('UnixAddress.to_string') fn (t) {
    t.equal(UnixAddress.new('foo.sock').to_string, 'foo.sock')
    t.equal(UnixAddress.new("\0foo").to_string, "\0foo")
    t.equal(UnixAddress.new('').to_string, '')
  }

  t.test('UnixAddress.abstract?') fn (t) {
    t.false(UnixAddress.new('').abstract?)
    t.false(UnixAddress.new('foo.sock').abstract?)
    t.true(UnixAddress.new("\0foo").abstract?)
  }

  t.test('UnixAddress.unnamed?') fn (t) {
    t.false(UnixAddress.new('foo.sock').unnamed?)
    t.false(UnixAddress.new("\0foo").unnamed?)
    t.true(UnixAddress.new('').unnamed?)
  }

  t.test('UnixAddress.fmt') fn (t) {
    t.equal(fmt(UnixAddress.new('foo.sock')), 'foo.sock')
    t.equal(fmt(UnixAddress.new("\0foo")), '@foo')
    t.equal(fmt(UnixAddress.new('')), 'unnamed')
  }

  t.test('UnixAddress.==') fn (t) {
    t.equal(UnixAddress.new('a.sock'), UnixAddress.new('a.sock'))
    t.not_equal(UnixAddress.new('a.sock'), UnixAddress.new('b.sock'))
  }

  t.test('UnixAddress.to_string') fn (t) {
    t.equal(UnixAddress.new('foo.sock').to_string, 'foo.sock')
    t.equal(UnixAddress.new("\0foo").to_string, "\0foo")
    t.equal(UnixAddress.new('').to_string, '')
  }

  if sys.unix? { unix_tests(t) }
}

fn unix_tests(t: mut Tests) {
  t.test('UnixSocket.new') fn (t) {
    t.no_throw fn { try UnixSocket.new(Type.DGRAM) }
    t.no_throw fn { try UnixSocket.new(Type.STREAM) }
  }

  t.test('UnixSocket.bind') fn (t) {
    let socket1 = try! UnixSocket.new(Type.STREAM)
    let socket2 = try! UnixSocket.new(Type.STREAM)
    let path = SocketPath.new(t.id)

    t.no_throw fn { try socket1.bind(path) }
    t.throw fn { try socket2.bind(path) }

    if sys.linux? {
      let socket = try! UnixSocket.new(Type.STREAM)

      t.no_throw fn { try socket.bind("\0inko-test-{t.id}") }
    }
  }

  t.test('UnixSocket.connect') fn (t) {
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)
    let path = SocketPath.new(t.id)

    t.throw fn { try stream.connect(path) }

    try! listener.bind(path)
    try! listener.listen

    t.no_throw fn { try stream.connect(path) }

    if sys.linux? {
      let listener = try! UnixSocket.new(Type.STREAM)
      let stream = try! UnixSocket.new(Type.STREAM)
      let addr = "\0inko-test-{t.id}"

      try! listener.bind(addr)
      try! listener.listen

      t.no_throw fn { try stream.connect(addr) }
    }
  }

  t.test('UnixSocket.listen') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixSocket.new(Type.STREAM)

    try! socket.bind(path)

    t.no_throw fn { try socket.listen }
  }

  t.test('UnixSocket.accept') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)

    let client = try! listener.accept

    t.equal(try! client.peer_address, try! stream.local_address)
  }

  t.test('UnixSocket.send_string_to') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixSocket.new(Type.DGRAM)

    try! socket.bind(path)
    try! socket.send_string_to('ping', path)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixSocket.send_bytes_to') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixSocket.new(Type.DGRAM)

    try! socket.bind(path)
    try! socket.send_bytes_to('ping'.to_byte_array, path)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixSocket.receive_from') fn (t) {
    let pair = SocketPath.pair(t.id)
    let listener = try! UnixSocket.new(Type.DGRAM)
    let client = try! UnixSocket.new(Type.DGRAM)

    try! listener.bind(pair.0)
    try! client.bind(pair.1)
    try! client.send_string_to('ping', pair.0)

    let bytes = ByteArray.new
    let sender = try! listener.receive_from(bytes: bytes, size: 4)

    t.equal(sender, try! client.local_address)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixSocket.local_address with an unbound socket') fn (t) {
    let socket = try! UnixSocket.new(Type.DGRAM)
    let address = try! socket.local_address

    t.equal(address, UnixAddress.new(''))
  }

  t.test('UnixSocket.local_address with a bound socket') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixSocket.new(Type.DGRAM)

    try! socket.bind(path)

    t.equal(try! socket.local_address, UnixAddress.new(path.to_string))
  }

  t.test('UnixSocket.peer_address with a disconnected socket') fn (t) {
    let socket = try! UnixSocket.new(Type.DGRAM)

    t.throw fn { try socket.peer_address }
  }

  t.test('UnixSocket.peer_address with a connected socket') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let client = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! client.connect(path)

    t.equal(try! client.peer_address, try! listener.local_address)
  }

  t.test('UnixSocket.read') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixSocket.new(Type.DGRAM)

    try! socket.bind(path)
    try! socket.send_string_to('ping', path)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixSocket.write_bytes') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)

    let written = try! stream.write_bytes('ping'.to_byte_array)
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixSocket.write_string') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)

    let written = try! stream.write_string('ping')
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixSocket.flush') fn (t) {
    let socket = try! UnixSocket.new(Type.STREAM)

    t.equal(socket.flush, nil)
  }

  t.test('UnixSocket.receive_buffer_size') fn (t) {
    let socket = try! UnixSocket.new(Type.STREAM)

    try! socket.receive_buffer_size = 256

    t.true(try! { socket.receive_buffer_size } >= 256)
  }

  t.test('UnixSocket.send_buffer_size') fn (t) {
    let socket = try! UnixSocket.new(Type.STREAM)

    try! socket.send_buffer_size = 256

    t.true(try! { socket.send_buffer_size } >= 256)
  }

  t.test('UnixSocket.shutdown_read') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)
    try! stream.shutdown_read

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 0)
    t.equal(bytes, ByteArray.new)
  }

  t.test('UnixSocket.shutdown_write') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)
    try! stream.shutdown_write

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('UnixSocket.shutdown shuts down the writing half') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)
    try! stream.shutdown

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('UnixSocket.shutdown shuts down the reading half') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)
    let stream = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen
    try! stream.connect(path)
    try! stream.shutdown

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 0)
    t.equal(bytes, ByteArray.new)
  }

  t.test('UnixSocket.try_clone') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixSocket.new(Type.STREAM)

    t.no_throw fn { try socket.try_clone }
  }

  t.test('UnixDatagram.new') fn (t) {
    let path = SocketPath.new(t.id)

    t.no_throw fn { try UnixDatagram.new(path) }
    t.throw fn { try UnixDatagram.new(path) }
  }

  t.test('UnixDatagram.connect') fn (t) {
    let pair = SocketPath.pair(t.id)
    let socket1 = try! UnixDatagram.new(pair.0)
    let socket2 = try! UnixDatagram.new(pair.1)

    t.no_throw fn { try socket2.connect(pair.0) }
  }

  t.test('UnixDatagram.send_to') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixDatagram.new(path)

    try! socket.send_string_to('ping', address: path)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixDatagram.receive_from') fn (t) {
    let pair = SocketPath.pair(t.id)
    let listener = try! UnixDatagram.new(pair.0)
    let client = try! UnixDatagram.new(pair.1)

    try! client.send_string_to('ping', pair.0)

    let bytes = ByteArray.new
    let sender = try! listener.receive_from(bytes: bytes, size: 4)

    t.equal(sender, try! client.local_address)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixDatagram.local_address') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixDatagram.new(path)

    t.equal(try! socket.local_address, UnixAddress.new(path.to_string))
  }

  t.test('UnixDatagram.try_clone') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixDatagram.new(path)

    t.no_throw fn { try socket.try_clone }
  }

  t.test('UnixDatagram.read') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixDatagram.new(path)

    try! socket.send_string_to('ping', address: path)

    let bytes = ByteArray.new

    t.equal(try! socket.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixDatagram.write_bytes') fn (t) {
    let pair = SocketPath.pair(t.id)
    let server = try! UnixDatagram.new(pair.0)
    let client = try! UnixDatagram.new(pair.1)

    try! client.connect(pair.0)

    let bytes = ByteArray.new

    t.equal(try! client.write_bytes('ping'.to_byte_array), 4)
    t.equal(try! server.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixDatagram.flush') fn (t) {
    let path = SocketPath.new(t.id)
    let socket = try! UnixDatagram.new(path)

    t.equal(socket.flush, nil)
  }

  t.test('UnixClient.new') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    t.no_throw fn { try UnixClient.new(path) }
  }

  t.test('UnixClient.with_timeout') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    # Unlike IP sockets there's no unroutable address we can use, so at best we
    # can assert the following doesn't time out. This means this test is mostly
    # a smoke test, unlikely to actually fail unless our runtime is bugged.
    t.no_throw fn {
      try UnixClient
        .with_timeout(address: path, timeout_after: Duration.from_secs(5))
    }
  }

  t.test('UnixClient.local_address') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    t.equal(try! stream.local_address, UnixAddress.new(''))
  }

  t.test('UnixClient.peer_address') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    t.equal(try! stream.peer_address, UnixAddress.new(path.to_string))
  }

  t.test('UnixClient.read') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)
    let connection = try! listener.accept

    try! connection.write_string('ping')

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixClient.write_bytes') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! stream.write_bytes('ping'.to_byte_array), 4)
    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixClient.write_string') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)
    let connection = try! listener.accept
    let bytes = ByteArray.new

    t.equal(try! stream.write_string('ping'), 4)
    t.equal(try! connection.read(into: bytes, size: 4), 4)
    t.equal(bytes.into_string, 'ping')
  }

  t.test('UnixClient.flush') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    t.equal(stream.flush, nil)
  }

  t.test('UnixClient.shutdown_read') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    try! stream.shutdown_read

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 0)
    t.equal(bytes, ByteArray.new)
  }

  t.test('UnixClient.shutdown_write') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    try! stream.shutdown_write

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('UnixClient.shutdown shuts down the writing half') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    try! stream.shutdown

    t.throw fn { try stream.write_string('ping') }
  }

  t.test('UnixClient.shutdown shuts down the reading half') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let stream = try! UnixClient.new(path)

    try! stream.shutdown

    let bytes = ByteArray.new

    t.equal(try! stream.read(into: bytes, size: 4), 0)
    t.equal(bytes, ByteArray.new)
  }

  t.test('UnixClient.try_clone') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixSocket.new(Type.STREAM)

    try! listener.bind(path)
    try! listener.listen

    let client = try! UnixClient.new(path)

    t.no_throw fn { try client.try_clone }
  }

  t.test('UnixServer.new') fn (t) {
    let path = SocketPath.new(t.id)

    t.no_throw fn { try UnixServer.new(path) }
  }

  t.test('UnixServer.accept') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixServer.new(path)
    let stream = try! UnixClient.new(path)
    let connection = try! listener.accept

    t.equal(try! connection.local_address, try! stream.peer_address)
  }

  t.test('UnixServer.local_address') fn (t) {
    let path = SocketPath.new(t.id)
    let listener = try! UnixServer.new(path)
    let addr = try! listener.local_address

    t.equal(addr, UnixAddress.new(path.to_string))
  }

  t.test('UnixServer.try_clone') fn (t) {
    let path = SocketPath.new(t.id)
    let server = try! UnixServer.new(path)

    t.no_throw fn { try server.try_clone }
  }
}
